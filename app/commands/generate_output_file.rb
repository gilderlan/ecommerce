class GenerateOutputFile
  prepend SimpleCommand

  attr_accessor :input_file, :errors

  def initialize(input_file)
    @input_file = input_file
    @errors = []
  end

  def call
    if valid?
      output
    else
      false
    end
  end

  def valid?
    @errors.push("Arquivo é obrigatório") if @input_file.nil?
    @errors.push("Arquivo não é um JSON") unless @input_file.try(:content_type).try(:eql?, 'application/json')
    @errors.push("Layout de arquivo inválido") unless valid_schema?
    @errors.empty?
  end

  def body
    return "" if @input_file.nil?
    IO.read @input_file.path
  end

  def json
    JSON.parse(body)
  end

  def valid_schema?
    schema = {
      type: "object",
      required: ["articles", "carts"],
      properties: {
        articles: {type: "array"},
        carts: {type: "array"},
        delivery_fees: {type: "array"},
        discounts: {type: "array"}
      }
    }

    JSON::Validator.validate(schema, body)
  end

  def output
    result = {carts: []}

    json['carts'].each do |cart|
      sum = sum_items(cart)
      discount_value = discount(cart)
      delivery_fee_value = delivery_fee(sum - discount_value)
      total = ((sum - discount_value) + delivery_fee_value).to_i

      result[:carts].push({id: cart['id'], total: total})
    end
    result
  end

  def sum_items(cart)
    sum = 0
    cart['items'].each do |item|
      price = json["articles"].select{|a| a["id"] == item["article_id"]}.first.try(:[], "price") || 0
      sum = sum + (price * item["quantity"])
    end
    sum
  end

  def delivery_fee(total)
    delivery_fees = json["delivery_fees"]
    return 0 if delivery_fees.nil?

    delivery_fee_policy = delivery_fees.select do |fee|
      transaction = fee["eligible_transaction_volume"]
      min = transaction.try(:[], "min_price") || 0
      max = transaction.try(:[], "max_price")

      break fee if (total >= min && max.nil?)
      break fee if (min..(max - 1)).member?(total)
      false
    end
    delivery_fee_policy.try(:[], "price") || 0
  end

  def discount(cart)
    discounts = json["discounts"]
    return 0.0 if discounts.nil?
    discount_total = 0.0

    cart['items'].each do |item|
      discount_policy = discounts.select{|d| d['article_id'] == item["article_id"]}.first
      unless discount_policy.nil?
        discount_value = discount_policy["value"].to_f
        if discount_policy["type"].eql? "amount"
          value = discount_value * item["quantity"]
          discount_total = discount_total + value
        else
          item_total = item_price(item) * item["quantity"]
          discount_total = discount_total + ((item_total * discount_value) / 100)
        end
      end
    end

    discount_total
  end

  def item_price(item)
    json["articles"].select{|a| a["id"] == item["article_id"]}.first["price"]
  end
end
