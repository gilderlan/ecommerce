class PagesController < ApplicationController
  def home
    @errors = []
  end

  def downloader

  end

  def upload
    output = GenerateOutputFile.call(params[:file])
    if output.success?
      send_data output.result.to_json, type: :json, disposition: 'attachment', filename: "output.json"
    else
      flash.now[:warning] = "Erro ao gerar arquivo de saída"
      @errors = output.errors
      render 'home'
    end
  end
end
