Rails.application.routes.draw do
  root "pages#home"
  match "/upload" => "pages#upload", :via => :post
end
