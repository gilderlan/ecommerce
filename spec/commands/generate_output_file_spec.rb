describe GenerateOutputFile do
  subject(:context) { described_class.call(file) }

  describe 'valid files' do
    context 'level_one file' do
      let(:file) {  Rack::Test::UploadedFile.new(File.open(File.join(Rails.root, '/spec/fixtures/input_files/level_one.json')), "application/json") }

      it 'succeeds' do
        expect(context).to be_success
      end
    end

    context 'level two' do
      let(:file) {  Rack::Test::UploadedFile.new(File.open(File.join(Rails.root, '/spec/fixtures/input_files/level_two.json')), "application/json") }

      it 'succeeds' do
        expect(context).to be_success
      end
    end

    context 'level three' do
      let(:file) {  Rack::Test::UploadedFile.new(File.open(File.join(Rails.root, '/spec/fixtures/input_files/level_three.json')), "application/json") }

      it 'succeeds' do
        expect(context).to be_success
      end
    end
  end

  describe 'invalid files' do
    context 'when file is not a json' do
      let(:file) {  Rack::Test::UploadedFile.new(File.open(File.join(Rails.root, '/spec/fixtures/input_files/level_one.txt'))) }

      it 'succeeds' do
        expect(context.errors).to include("Arquivo não é um JSON")
      end
    end

    context 'when file json layout is invalid' do
      let(:file) {  Rack::Test::UploadedFile.new(File.open(File.join(Rails.root, '/spec/fixtures/input_files/level_one_invalid.json'))) }

      it 'succeeds' do
        expect(context.errors).to include("Layout de arquivo inválido")
      end
    end

    context 'when input_file is empty' do
      let(:file) { nil }

      it 'succeeds' do
        expect(context.errors).to include("Arquivo é obrigatório")
      end
    end
  end

  describe 'Calculation methods' do
    let(:input_level_one) {GenerateOutputFile.new Rack::Test::UploadedFile.new(File.open(File.join(Rails.root, '/spec/fixtures/input_files/level_one.json')), "application/json")}
    let(:input_level_two) {GenerateOutputFile.new Rack::Test::UploadedFile.new(File.open(File.join(Rails.root, '/spec/fixtures/input_files/level_two.json')), "application/json")}
    let(:input_level_three) {GenerateOutputFile.new Rack::Test::UploadedFile.new(File.open(File.join(Rails.root, '/spec/fixtures/input_files/level_three.json')), "application/json")}

    context 'sum' do
      it 'should sum itens' do
        cart = {
          "id": 1,
          "items": [
            { "article_id": 1, "quantity": 6 },
            { "article_id": 2, "quantity": 2 },
            { "article_id": 4, "quantity": 2 }
          ]
        }
        cart = JSON.parse cart.to_json
        expect(input_level_one.sum_items(cart)).to eq(3000)
      end

      it 'should sum only itens with price' do
        cart = {
          "id": 1,
          "items": [
            { "article_id": 1, "quantity": 6 },
            { "article_id": 2, "quantity": 2 },
            { "article_id": 7, "quantity": 2 }
          ]
        }
        cart = JSON.parse cart.to_json
        expect(input_level_one.sum_items(cart)).to eq(1000)
      end

      it 'should return zero when items is empty' do
        cart = {
          "id": 1,
          "items": []
        }
        cart = JSON.parse cart.to_json
        expect(input_level_one.sum_items(cart)).to eq(0)
      end
    end

    context 'delivery_fee' do
      it 'should return zero when delivery_fee table is empty' do
        expect(input_level_one.delivery_fee(2000)).to eq(0)
      end

      it 'should calculate delivery_fee' do
        expect(input_level_two.delivery_fee(0)).to eq(800)
        expect(input_level_two.delivery_fee(300)).to eq(800)
        expect(input_level_two.delivery_fee(1000)).to eq(400)
        expect(input_level_two.delivery_fee(10000)).to eq(0)
      end
    end

    context 'discount' do
      it 'should return zero when discount table is empty' do
        cart = JSON.parse ({"id": 1,"items": [{ "article_id": 2, "quantity": 10 }]}.to_json)
        expect(input_level_one.discount(cart)).to eq(0.0)
      end

      it 'should calculate discount' do
        cart_without_discount = JSON.parse ({"id": 1,"items": [{ "article_id": 1, "quantity": 6 }]}.to_json)
        cart_with_amount_discount = JSON.parse ({"id": 1,"items": [{ "article_id": 2, "quantity": 2 }]}.to_json)
        cart_with_percent_discount = JSON.parse ({"id": 1,"items": [{ "article_id": 8, "quantity": 2 }]}.to_json)

        expect(input_level_three.discount(cart_without_discount)).to eq(0.0)
        expect(input_level_three.discount(cart_with_amount_discount)).to eq(50.0)
        expect(input_level_three.discount(cart_with_percent_discount)).to eq(29.4)
      end
    end
  end

end
